import { plane } from '../plane';
import { Component, OnInit, Input } from '@angular/core';
import { PlaneService } from '../plane.service';
import { PlaneListComponent } from '../plane-list/plane-list.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-plane-details',
  templateUrl: './plane-details.component.html',
  styleUrls: ['./plane-details.component.css']
})
export class PlaneDetailsComponent implements OnInit {

  serviceNumber: number;
  plane: plane;

  constructor(private route: ActivatedRoute,private router: Router,
    private planeService: PlaneService) { }

  ngOnInit() {
    this.plane = new plane();

    this.serviceNumber = this.route.snapshot.params['serviceNumber'];
    
    this.planeService.getPlane(this.serviceNumber)
      .subscribe(data => {
        console.log(data)
        this.plane = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['planes']);
  }
}