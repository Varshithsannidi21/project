import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { pilot } from './pilot';

@Injectable({
  providedIn: 'root'
})
export class PilotService {

  private baseUrl = 'http://localhost:8084/AirportManagmentSystem/api';

  constructor(private http: HttpClient) { }

  getPilot(pilotId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${pilotId}`);

  }

  createPilot(pilot: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, pilot);
  }

  updatePilot(pilotId: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${pilotId}`, value);
  }

  deletePilot(pilotId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${pilotId}`, { responseType: 'text' });
  }

  getPilotList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  
  
}
