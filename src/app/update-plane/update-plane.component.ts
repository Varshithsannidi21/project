import { Component, OnInit } from '@angular/core';
import { plane } from '../plane';


@Component({
  selector: 'app-update-plane',
  templateUrl: './update-plane.component.html',
  styleUrls: ['./update-plane.component.css']
})
export class UpdatePlaneComponent implements OnInit {
  submitted: boolean;
  plane: plane =new plane();

  constructor() { }


  ngOnInit(): void {
  }
  newPlane(): void {
    this.submitted = false;
    this.plane = new plane();
  }

  onSubmit() {
    this.submitted = true;
    this.update();    
  }
  update() {
    throw new Error("Method not implemented.");
  }

}
