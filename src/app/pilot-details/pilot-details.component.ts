import { pilot } from '../pilot';
import { Component, OnInit, Input } from '@angular/core';
import { PilotService } from '../pilot.service';
import { PilotListComponent } from '../pilot-list/pilot-list.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pilot-details',
  templateUrl: './pilot-details.component.html',
  styleUrls: ['./pilot-details.component.css']
})
export class PilotDetailsComponent implements OnInit {

  pilotId: number;
  pilot: pilot;

  constructor(private route: ActivatedRoute,private router: Router,
    private pilotService: PilotService) { }

  ngOnInit() {
    this.pilot = new pilot();

    this.pilotId = this.route.snapshot.params['pilotId'];
    
    this.pilotService.getPilot(this.pilotId)
      .subscribe(data => {
        console.log(data)
        this.pilot = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['pilots']);
  }
}