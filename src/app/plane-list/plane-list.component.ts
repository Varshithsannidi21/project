import { Observable } from "rxjs";
import { PlaneService } from "../plane.service";
import { plane } from "../plane";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-plane-list",
  templateUrl: "./plane-list.component.html",
  styleUrls: ["./plane-list.component.css"]
})
export class PlaneListComponent implements OnInit {
  plane: Observable<plane[]>;
  planeService: PlaneService;

  constructor(private pilotService: PlaneService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.plane = this.planeService.getPlaneList();
  }

  deletePlane(serviceNumber: number) {
    this.planeService.deletePlane(serviceNumber)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  planeDetails(serviceNumber: number){
    this.router.navigate(['detailsPlane', serviceNumber]);
  }
  
}