import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HangerListComponent } from './hanger-list.component';

describe('HangerListComponent', () => {
  let component: HangerListComponent;
  let fixture: ComponentFixture<HangerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HangerListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HangerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
