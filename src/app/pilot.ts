export class pilot{

    pilotId : number;
	firstName :string;
	lastName :string;
	age: number;
	gender: string;
	eMail: string;
	contactNumber :number;
    experience: number;
    active: boolean;
}