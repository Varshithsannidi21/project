import { PilotService } from '../pilot.service';
import { pilot } from '../pilot';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-pilot',
  templateUrl: './create-pilot.component.html',
  styleUrls: ['./create-pilot.component.css']
})
export class CreatePilotComponent implements OnInit {

  pilot: pilot = new pilot();
  submitted = false;

  constructor(private pilotService: PilotService,
    private router: Router) { }

  ngOnInit() {
  }

  newPilot(): void {
    this.submitted = false;
    this.pilot = new pilot();
  }

  save() {
    this.pilotService
    .createPilot(this.pilot).subscribe(data => {
      console.log(data)
      this.pilot = new pilot();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
    
  }

  gotoList() {
    this.router.navigate(['/pilots']);
  }
}