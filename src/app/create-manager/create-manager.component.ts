import { ManagerService } from '../manager.service';
import { manager } from '../manager';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-manager',
  templateUrl: './create-manager.component.html',
  styleUrls: ['./create-manager.component.css']
})
export class CreateManagerComponent implements OnInit {

  manager: manager = new manager();
  submitted = false;

  constructor(private managerService: ManagerService,
    private router: Router) { }

  ngOnInit() {
  }

  newPilot(): void {
    this.submitted = false;
    this.manager = new manager();
  }

  save() {
    this.managerService
    .createManager(this.manager).subscribe(data => {
      console.log(data)
      this.manager = new manager();
     
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

 
}