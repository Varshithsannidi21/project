import { AdminService } from '../admin.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { admin } from '../Admin';
@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {

  admin: admin = new admin();
  submitted = false;

  constructor(private adminService: AdminService,
    private router: Router) { }

  ngOnInit() {
  }

  newAdmin(): void {
    this.submitted = false;
    this.admin = new admin();
  }

  save() {
    this.adminService
    .createAdmin(this.admin).subscribe(data => {
      console.log(data)
      this.admin = new admin();
     
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

 
}