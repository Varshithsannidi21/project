import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatePilotComponent } from './create-pilot/create-pilot.component';
import { PilotDetailsComponent } from './pilot-details/pilot-details.component';
import { PilotListComponent } from './pilot-list/pilot-list.component';
import { UpdatePilotComponent } from './update-pilot/update-pilot.component';
import { HttpClientModule } from '@angular/common/http';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { UpdateAdminComponent } from './update-admin/update-admin.component';

import { AdminLoginComponent } from './admin-login/admin-login.component';
import { ManagerLoginComponent } from './manager-login/manager-login.component';
import { CreateManagerComponent } from './create-manager/create-manager.component';
import { CreateHangerComponent } from './create-hanger/create-hanger.component';
import { UpdateHangerComponent } from './update-hanger/update-hanger.component';
import { HangerDetailsComponent } from './hanger-details/hanger-details.component';
import { HangerListComponent } from './hanger-list/hanger-list.component';
import { CreatePlaneComponent } from './create-plane/create-plane.component';
import { PlaneListComponent } from './plane-list/plane-list.component';
import { PlaneDetailsComponent } from './plane-details/plane-details.component';
import { UpdatePlaneComponent } from './update-plane/update-plane.component';
import { DisplayPilotComponent } from './display-pilot/display-pilot.component';
import { DisplayPlaneComponent } from './display-plane/display-plane.component';
import { DisplayAdminComponent } from './display-admin/display-admin.component';
import { DisplayManagerComponent } from './display-manager/display-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    CreatePilotComponent,
    PilotDetailsComponent,
    PilotListComponent,
    UpdatePilotComponent,
    CreateAdminComponent,
    UpdateAdminComponent,
   
    AdminLoginComponent,
    ManagerLoginComponent,
    CreateManagerComponent,
    CreateHangerComponent,
    UpdateHangerComponent,
    HangerDetailsComponent,
    HangerListComponent,
    CreatePlaneComponent,
    PlaneListComponent,
    PlaneDetailsComponent,
    UpdatePlaneComponent,
    DisplayPilotComponent,
    DisplayPlaneComponent,
    DisplayAdminComponent,
    DisplayManagerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
