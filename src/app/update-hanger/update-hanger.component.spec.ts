import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateHangerComponent } from './update-hanger.component';

describe('UpdateHangerComponent', () => {
  let component: UpdateHangerComponent;
  let fixture: ComponentFixture<UpdateHangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateHangerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateHangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
