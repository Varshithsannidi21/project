import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-plane',
  templateUrl: './display-plane.component.html',
  styleUrls: ['./display-plane.component.css']
})
export class DisplayPlaneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = '**PLANES**';

}
