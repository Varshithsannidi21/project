import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePilotComponent } from './create-pilot/create-pilot.component';
import { UpdatePilotComponent } from './update-pilot/update-pilot.component';
import { PilotDetailsComponent } from './pilot-details/pilot-details.component';
import { PilotListComponent } from './pilot-list/pilot-list.component';
import { PlaneListComponent } from './plane-list/plane-list.component';
import { CreatePlaneComponent } from './create-plane/create-plane.component';
import { UpdatePlaneComponent } from './update-plane/update-plane.component';
import { PlaneDetailsComponent } from './plane-details/plane-details.component';
import { DisplayPilotComponent } from './display-pilot/display-pilot.component';
import { DisplayPlaneComponent } from './display-plane/display-plane.component';
import { DisplayAdminComponent } from './display-admin/display-admin.component';
import { DisplayManagerComponent } from './display-manager/display-manager.component';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { CreateManagerComponent } from './create-manager/create-manager.component';

const routes: Routes = [
  { path: '', redirectTo: 'pilot', pathMatch: 'full' },
  
  {path :'displaymanager', component: DisplayManagerComponent},
  {path :'displayadmin', component: DisplayAdminComponent},
  {path :'displaypilot', component: DisplayPilotComponent},
  { path: 'add', component: CreatePilotComponent },
  { path: 'addAdmin', component: CreateAdminComponent },
  { path: 'addManager', component: CreateManagerComponent },
  { path: 'pilots', component: PilotListComponent },
  {path: 'displayplane',component:DisplayPlaneComponent},
  { path: 'addPlane', component: CreatePlaneComponent },
  { path: 'planes', component: PlaneListComponent },
  { path: 'update/:pilotId', component: UpdatePilotComponent },
  { path: 'details/:pilotId', component: PilotDetailsComponent }, 
  { path: 'updatePlane/:serviceNumber', component: UpdatePlaneComponent},
  { path: 'detailsPlane/:serviceNumber', component: PlaneDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
