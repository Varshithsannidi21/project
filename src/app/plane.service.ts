import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { plane } from './plane';

@Injectable({
  providedIn: 'root'
})
export class PlaneService {

  private baseUrl = 'http://localhost:8084/api';

  constructor(private http: HttpClient) { }

  getPlane(serviceNumber: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${serviceNumber}`);
  }

  createPlane(plane: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, plane);
  }

  updatePlane(serviceNumber: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${serviceNumber}`, value);
  }

  deletePlane(serviceNumber: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${serviceNumber}`, { responseType: 'text' });
  }

  getPlaneList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}