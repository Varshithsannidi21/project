import { Component, OnInit } from '@angular/core';
import { pilot } from '../pilot';

@Component({
  selector: 'app-update-pilot',
  templateUrl: './update-pilot.component.html',
  styleUrls: ['./update-pilot.component.css']
})
export class UpdatePilotComponent implements OnInit {
  submitted: boolean;
  pilot: pilot =new pilot();

  constructor() { }

  ngOnInit(): void {
  }
  newPilot(): void {
    this.submitted = false;
    this.pilot = new pilot();
  }

  onSubmit() {
    this.submitted = true;
    this.update();    
  }
  update() {
    throw new Error("Method not implemented.");
  }

}
