import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HangerDetailsComponent } from './hanger-details.component';

describe('HangerDetailsComponent', () => {
  let component: HangerDetailsComponent;
  let fixture: ComponentFixture<HangerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HangerDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HangerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
