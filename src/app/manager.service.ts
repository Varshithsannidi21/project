import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { manager } from './manager';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private baseUrl = 'http://localhost:8084/AirportManagmentSystem/api';

  constructor(private http: HttpClient) { }


  createManager(manager: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, manager);
  }

}
