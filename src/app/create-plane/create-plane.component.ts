import { PlaneService } from '../plane.service';
import { plane } from '../plane';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-plane',
  templateUrl: './create-plane.component.html',
  styleUrls: ['./create-plane.component.css']
})
export class CreatePlaneComponent implements OnInit {

  plane: plane = new plane();
  submitted = false;

  constructor(private planeService: PlaneService,
    private router: Router) { }

  ngOnInit() {
  }

  newPilot(): void {
    this.submitted = false;
    this.plane = new plane();
  }

  save() {
    this.planeService
    .createPlane(this.plane).subscribe(data => {
      console.log(data)
      this.plane = new plane();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/planes']);
  }
}