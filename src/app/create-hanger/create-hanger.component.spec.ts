import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHangerComponent } from './create-hanger.component';

describe('CreateHangerComponent', () => {
  let component: CreateHangerComponent;
  let fixture: ComponentFixture<CreateHangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateHangerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
