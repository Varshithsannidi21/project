import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-admin',
  templateUrl: './display-admin.component.html',
  styleUrls: ['./display-admin.component.css']
})
export class DisplayAdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'AirportManagmentSystem';

}
