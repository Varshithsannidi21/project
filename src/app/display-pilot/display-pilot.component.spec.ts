import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPilotComponent } from './display-pilot.component';

describe('DisplayPilotComponent', () => {
  let component: DisplayPilotComponent;
  let fixture: ComponentFixture<DisplayPilotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayPilotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayPilotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
