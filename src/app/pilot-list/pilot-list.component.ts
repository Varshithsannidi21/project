import { Observable } from "rxjs";
import { PilotService } from "../pilot.service";
import { pilot } from "../pilot";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-pilot-list",
  templateUrl: "./pilot-list.component.html",
  styleUrls: ["./pilot-list.component.css"]
})
export class PilotListComponent implements OnInit {
  pilot: Observable<pilot[]>;

  constructor(private pilotService: PilotService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.pilot = this.pilotService.getPilotList();
  }

  deletePilot(pilotId: number) {
    this.pilotService.deletePilot(pilotId)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  pilotDetails(pilotId: number){
    this.router.navigate(['details', pilotId]);
  }
  
}