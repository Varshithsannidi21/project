package com.pack.AirportManagmentSystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hangar")
public class Hangars {

	@Id
	
	private int hangarId;
	public Hangars(String hangarName, String hangarStatus, String location) {
		super();
		this.hangarName = hangarName;
		this.hangarStatus = hangarStatus;
		this.location = location;
	}

	public Hangars(int hangarId, String hangarName, String hangarStatus, String location) {
		super();
		this.hangarId = hangarId;
		this.hangarName = hangarName;
		this.hangarStatus = hangarStatus;
		this.location = location;
	}

	private String hangarName;
	private String hangarStatus;
	private String location;

	public String getHangarName() {
		return hangarName;
	}

	public void setHangarName(String hangarName) {
		this.hangarName = hangarName;
	}

	public String getHangarStatus() {
		return hangarStatus;
	}

	public void setHangarStatus(String hangarStatus) {
		this.hangarStatus = hangarStatus;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Hangars [hangarName=" + hangarName + ", hangarStatus=" + hangarStatus + ", location=" + location + "]";
	}

	public Hangars() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
