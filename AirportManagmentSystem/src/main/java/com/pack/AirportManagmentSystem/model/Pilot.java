package com.pack.AirportManagmentSystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pilot")
public class Pilot {

	public int getPilotId() {
		return pilotId;
	}

	public void setPilotId(int pilotId) {
		this.pilotId = pilotId;
	}

	@Id

	private int pilotId;
	public Pilot(String firstName, String lastName, int age, String eMail, long contactNumber, int experience) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.eMail = eMail;
		this.contactNumber = contactNumber;
		this.experience = experience;
	}

	private String firstName;
	private String lastName;
	private int age;
	private String eMail;
	private long contactNumber;
	private int experience;

	public Pilot(int pilotId, String firstName, String lastName, int age, String eMail, long contactNumber,
			int experience) {
		super();
		this.pilotId = pilotId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.eMail = eMail;
		this.contactNumber = contactNumber;
		this.experience = experience;
	}

	public Pilot() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@Override
	public String toString() {
		return "Pilot [pilotId=" + pilotId + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
				+ ", eMail=" + eMail + ", contactNumber=" + contactNumber + ", experience=" + experience + "]";
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public int getExperience() {
		return experience;

	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

}
