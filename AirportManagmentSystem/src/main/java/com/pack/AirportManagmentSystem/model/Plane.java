package com.pack.AirportManagmentSystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "plane")
public class Plane {

	public Plane(int serviceNumber, String name, String modelNumber, String type, String company) {
		super();
		this.serviceNumber = serviceNumber;
		this.name = name;
		this.modelNumber = modelNumber;
		this.type = type;
		this.company = company;
	}

	public Plane(String name, String modelNumber, String type, String company) {
		super();
		this.name = name;
		this.modelNumber = modelNumber;
		this.type = type;
		this.company = company;
	}

	@Id
	private int serviceNumber;
	private String name;
	private String modelNumber;
	private String type;
	private String company;

	public int getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(int serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public Plane() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Plane [serviceNumber=" + serviceNumber + ", name=" + name + ", modelNumber=" + modelNumber + ", type="
				+ type + ", company=" + company + "]";
	}

}
