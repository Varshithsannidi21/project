package com.pack.AirportManagmentSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirportManagmentSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirportManagmentSystemApplication.class, args);
	}
	
}
