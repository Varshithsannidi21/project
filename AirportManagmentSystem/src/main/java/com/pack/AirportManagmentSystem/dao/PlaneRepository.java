package com.pack.AirportManagmentSystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pack.AirportManagmentSystem.model.Plane;

@Repository
public interface PlaneRepository extends JpaRepository<Plane, Long> {

}
