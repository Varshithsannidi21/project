package com.pack.AirportManagmentSystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pack.AirportManagmentSystem.model.Hangars;

@Repository
public interface HangarRepository extends JpaRepository<Hangars, Long> {

}
