package com.pack.AirportManagmentSystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pack.AirportManagmentSystem.model.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

	Admin save(Admin admin);

}
