package com.pack.AirportManagmentSystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pack.AirportManagmentSystem.model.Pilot;

@Repository
public interface PilotRepository extends JpaRepository<Pilot, Long> {

}
