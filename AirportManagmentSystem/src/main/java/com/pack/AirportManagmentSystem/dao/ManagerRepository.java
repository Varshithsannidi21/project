package com.pack.AirportManagmentSystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pack.AirportManagmentSystem.model.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {

}
