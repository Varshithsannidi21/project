package com.pack.AirportManagmentSystem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.AirportManagmentSystem.dao.AdminRepository;
import com.pack.AirportManagmentSystem.dao.HangarRepository;
import com.pack.AirportManagmentSystem.dao.ManagerRepository;
import com.pack.AirportManagmentSystem.dao.PilotRepository;
import com.pack.AirportManagmentSystem.dao.PlaneRepository;
import com.pack.AirportManagmentSystem.exception.ResourceNotFoundException;
import com.pack.AirportManagmentSystem.model.Admin;
import com.pack.AirportManagmentSystem.model.Hangars;
import com.pack.AirportManagmentSystem.model.Manager;
import com.pack.AirportManagmentSystem.model.Pilot;
import com.pack.AirportManagmentSystem.model.Plane;

@RestController
@CrossOrigin(origins = "http://localhost:4206")
@RequestMapping("/api")
public class PilotController {
	@Autowired
	PilotRepository pilotRepository;

	@Autowired
	AdminRepository adminRepository;

	@Autowired
	PlaneRepository planeRepository;

	@Autowired
	ManagerRepository managerRepository;

	@Autowired
	HangarRepository hangarRepository;

	@PostMapping("/planereg")
	public Plane createPlane(@RequestBody Plane plane) {
		return planeRepository.save(plane);

	}

	@PostMapping("/AirportManagmentSystem")
	public Admin createAdmin(@RequestBody Admin admin) {
		return adminRepository.save(admin);

	}

	@PostMapping("/managerreg")
	public Manager createManager(@RequestBody Manager manager) {
		return managerRepository.save(manager);
	}

	@PostMapping("/add")
	public Pilot createPilot(@RequestBody Pilot pilot) {
		return pilotRepository.save(pilot);
	}

	@GetMapping("/planes")
	public List<Plane> getAllPlanes() {
		return (List<Plane>) planeRepository.findAll();
	}

	@GetMapping("/hangars")
	public List<Hangars> getAllHangars() {
		return (List<Hangars>) hangarRepository.findAll();
	}

	@GetMapping("/pilots")
	public List<Pilot> getAllPilot() {
		return (List<Pilot>) pilotRepository.findAll();
	}

	@GetMapping("/pilot/{pilotId}")
	public ResponseEntity<Pilot> getPilotById(@PathVariable(value = "pilotId") Long pilotId)
			throws ResourceNotFoundException {
		Pilot pilot = pilotRepository.findById(pilotId)
				.orElseThrow(() -> new ResourceNotFoundException("Pilot not found for this id :: " + pilotId));
		return ResponseEntity.ok().body(pilot);
	}

	@GetMapping("/plane/{serviceNumber}")
	public ResponseEntity<Plane> getPlaneById(@PathVariable(value = "serviceNumber") Long serviceNumber)
			throws ResourceNotFoundException {
		Plane plane = planeRepository.findById(serviceNumber).orElseThrow(
				() -> new ResourceNotFoundException("Pilot not found for this serviceNumber :: " + serviceNumber));
		return ResponseEntity.ok().body(plane);
	}

	@PutMapping("/planes/{serviceNumber}")
	public ResponseEntity<Plane> updatePlane(@PathVariable(value = "serviceNumber") Long serviceNumber,
			@RequestBody Plane planeDetails) throws ResourceNotFoundException {
		Plane plane = planeRepository.findById(serviceNumber)
				.orElseThrow(() -> new ResourceNotFoundException("pilot not found for this id :: " + serviceNumber));

		plane.setName(planeDetails.getName());
		plane.setModelNumber(planeDetails.getModelNumber());
		plane.setCompany(planeDetails.getCompany());
		final Plane updatedPlane = planeRepository.save(plane);
		return ResponseEntity.ok(updatedPlane);
	}

	@DeleteMapping("/planes/{serviceNumber}")

	public ResponseEntity<Plane> deletePlane(@PathVariable("serviceNumber") long serviceNumber) {
		try {
			planeRepository.deleteById(serviceNumber);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PutMapping("/pilots/{pilotId}")
	public ResponseEntity<Pilot> updatePilot(@PathVariable(value = "pilotId") Long pilotId,
			@RequestBody Pilot pilotDetails) throws ResourceNotFoundException {
		Pilot pilot = pilotRepository.findById(pilotId)
				.orElseThrow(() -> new ResourceNotFoundException("pilot not found for this id :: " + pilotId));

		pilot.seteMail(pilotDetails.geteMail());
		pilot.setLastName(pilotDetails.getLastName());
		pilot.setFirstName(pilotDetails.getFirstName());
		final Pilot updatedPilot = pilotRepository.save(pilot);
		return ResponseEntity.ok(updatedPilot);
	}

	@DeleteMapping("/pilots/{pilotId}")

	public ResponseEntity<Pilot> deleteCustomer(@PathVariable("pilotId") long pilotId) {
		try {
			pilotRepository.deleteById(pilotId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
